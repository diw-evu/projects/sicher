#%% Libraries ######################################################################################

import pandas as pd
import numpy as np
import os
import openpyxl
pd.options.plotting.backend = "plotly"

# Import data

from helper import * 

#%% Create base time series sheet ##################################################################

# Select whether one- or two-year period to be used
period_length = 1

# Select whether academic or calendar year used
year_calendar = False
#selected_coutries = ['AT','BE','CH','CZ','DE','DK','ES','FI','FR','IE','IT','LU','NL','NO','PL','PT','SE','UK']

# Select countries
selected_coutries = ['DE']
selected_base_year = '2012-2013'

"""
# Select base year and iteration years
if year_calendar == True:  
    selected_base_year = 2008
    selected_years = [y for y in range(1982, 2019)]
    
if year_calendar == False:
    selected_base_year = "2008-2009"
    selected_years = [
        "1990-1991",
        "1991-1992",
        "1992-1993",
        "1993-1994",
        "1994-1995",
        "1995-1996",
        "1996-1997",
        "1997-1998",
        "1998-1999",
        "1999-2000",
        "2000-2001",
        "2001-2002",
        "2002-2003",
        "2003-2004",
        "2004-2005",
        "2005-2006",
        "2006-2007",
        "2007-2008",
        "2008-2009",
        "2009-2010",
        "2010-2011"
        ]
"""

#%% Create base data

# Get data - execute function
base_ts = create_timeseries_input(data_dict,selected_base_year,selected_coutries,year_calendar,period_length)

# Scale to 715 TWh
demand_sum = base_ts.query("headers_time == 'demand'").value.sum() / 1e6
scale      = 715 / demand_sum
scaled_ts  = base_ts.query("headers_time == 'demand'").value * scale
base_ts.loc[base_ts.eval("headers_time == 'demand'"), 'value'] = scaled_ts

#%%

# To wide format
sel_ts = base_ts.pivot(index=['headers_time','n'], columns="h2", values='value')

# Export to csv
sel_ts.to_csv(os.path.join("project_files","data_input","data_input_wide","0_Core","time_data_upload.csv"))

#%%
