import requests, zipfile, io, os

from pathlib import Path

# Demand data ##############################

Path(os.path.join("data","demand")).mkdir(parents=True, exist_ok=True)

print("Start download of demand data")

r = requests.get("https://eepublicdownloads.blob.core.windows.net/public-cdn-container/clean-documents/sdc-documents/ERAA/2023/Demand%20Dataset.zip")

print("Finished")

z = zipfile.ZipFile(io.BytesIO(r.content))
z.extractall(os.path.join("data","demand"))

# PECD #####################################

Path(os.path.join("data","PECD")).mkdir(parents=True, exist_ok=True)

print("Start download of PECD data")

r = requests.get("https://eepublicdownloads.blob.core.windows.net/public-cdn-container/clean-documents/sdc-documents/ERAA/2023/PECD.zip")

print("Finished")

z = zipfile.ZipFile(io.BytesIO(r.content))
z.extractall(os.path.join("data"))