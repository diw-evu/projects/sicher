Parameter

capacity_market_req(n)
;

capacity_market_req(n)  = 100000;

con_capacity(n).. capacity_market_req(n)       =L= N_TECH(n,"CCGT") + N_TECH(n,"OCGT") + N_TECH(n,"oil") + N_TECH(n,"bio") + N_STO_P_OUT(n,"p2g2p") + N_STO_P_OUT(n,"rsvr");
con_capacity_bat(n).. capacity_market_req(n)   =L= N_TECH(n,"CCGT") + N_TECH(n,"OCGT") + N_TECH(n,"oil") + N_TECH(n,"bio") + N_STO_P_OUT(n,"p2g2p") + N_STO_P_OUT(n,"rsvr") +        N_STO_P_OUT(n,"li-ion");
con_capacity_bat25(n).. capacity_market_req(n) =L= N_TECH(n,"CCGT") + N_TECH(n,"OCGT") + N_TECH(n,"oil") + N_TECH(n,"bio") + N_STO_P_OUT(n,"p2g2p") + N_STO_P_OUT(n,"rsvr") + 0.25 * N_STO_P_OUT(n,"li-ion");
