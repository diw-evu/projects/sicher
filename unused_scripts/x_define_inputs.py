#%% Load packages

import os
import pandas as pd
import numpy as np

#%% TECHNOLOGY #####################################################################################

#%% ERAA data

eraa_capas = pd.read_csv(os.path.join('data','eraa2021','capacities-national-estimates.csv'))

# Drop doubles for DE and IT

eraa_capas = eraa_capas.query("area != ['DEKF','ITCN','ITCS','ITN1','ITS1','ITSA','ITSI']")

# Sum countries

eraa_capas_agg = eraa_capas.groupby(['country','tech','scenario']).agg({'total_country':'sum'}).reset_index()

# Rename

eraa_capas_agg.loc[:,'tech2'] = eraa_capas_agg.loc[:,'tech']

eraa_capas_agg.loc[:,'tech'] = eraa_capas_agg.loc[:,'tech'].replace(
    {
        'Biofuel':'bio',
        'Gas': 'CCGT',
        'Hard Coal': 'hc',
        'Hydro - Run of River & Pondage (Turbine)' : 'ror',
        'Lignite':'lig',
        'Nuclear':'nuc',
        'Oil':'oil',
        'Others non-renewable':'other',
        'Others renewable':'bio',
        'Solar (Photovoltaic)':'pv',
        'Solar (Thermal)':'pv',
        'Wind Offshore':'wind_off',
        'Wind Onshore':'wind_on' 
        }
)

# Filter and combine
eraa_capas_agg = eraa_capas_agg.query("scenario == 'National Estimates 2025'")
eraa_tech      = eraa_capas_agg.query("tech != ['phs_closed','phs_open','rsvr']").reset_index(drop=True)
eraa_tech      = eraa_tech.groupby(['country','tech','scenario']).agg({'total_country':'sum'}).reset_index()
eraa_tech.loc[:,'total_country'] = round(eraa_tech.loc[:,'total_country'],2)

# Set lower and upper bounds to "current"
eraa_tech.loc[:,"min"] = eraa_tech.loc[:,"total_country"]
eraa_tech.loc[:,"max"] = eraa_tech.loc[:,"total_country"]

# Set oil and other
eraa_tech.loc[eraa_tech.loc[:,"tech"].isin(['oil','other']),"min"] = 0
#eraa_tech.loc[eraa_tech.loc[:,"tech"].isin(['oil','other']),"max"] = 0

# Nuc (do not change)

# Set wind, pv, gas
eraa_tech.loc[:,"max"] = eraa_tech.loc[:,"max"].astype(str)
eraa_tech.loc[eraa_tech.loc[:,"tech"].isin(['wind_on','wind_off','pv','CCGT']),"max"] = "inf"

#%% EMBER data

# ember_url = "https://ember-climate.org/app/uploads/2022/07/yearly_full_release_long_format.csv"

# Import
ember_data = pd.read_csv(os.path.join("data","ember","yearly_full_release_long_format.csv"))
ember_data = ember_data.rename(columns={"Country code":"n3"})  

# Add country codes
country_codes = pd.read_csv(os.path.join("data","country_codes.csv"), sep = ";")
country_codes = country_codes[["Alpha-3 code","Alpha-2 code"]].rename(
    columns={"Alpha-2 code":"n2","Alpha-3 code":"n3"})
country_codes["n2"] = country_codes["n2"].str.strip()
country_codes["n3"] = country_codes["n3"].str.strip()

ember_data = pd.merge(ember_data, country_codes, how = "left", on="n3")

# Select generation bioenergy

ember_data_filtered = ember_data.query(
    "Year == 2022 & Category == 'Electricity generation' & Variable == 'Bioenergy' & Unit == 'TWh'")

ember_data_filtered = ember_data_filtered[['n2','Value']].dropna().reset_index(drop=True)
ember_data_filtered["Value"] *= 1e6
ember_data_filtered = ember_data_filtered.rename(
    columns={"n2":"n", "Value":"max_energy_import"})
ember_data_filtered["n"] = ember_data_filtered["n"].replace(
    {"GB":"UK"})
ember_data_filtered["tech"] = "bio"

#%% Import tech_data

tech_data = pd.read_csv(os.path.join(
    'project_files','data_input','data_input_wide','0_Core',
    'technology_data_upload.csv'))

#%% Merge with ERAA
tech_data_merged = tech_data.merge(eraa_tech, how = "left", left_on=['n','tech'], right_on=['country','tech'])
tech_data_merged.loc[:,'min_installable'] = tech_data_merged.loc[:,'min']
tech_data_merged.loc[:,'max_installable'] = tech_data_merged.loc[:,'max']

# Drop columns
tech_data_merged = tech_data_merged.drop(columns=['country','scenario','total_country','min','max'])

#%% Merge with EMBER
tech_data_merged = tech_data_merged.merge(ember_data_filtered, how = "left", on = ["n","tech"])
tech_data_merged.loc[:,'max_energy'] = tech_data_merged.loc[:,'max_energy_import']
tech_data_merged.loc[:,'max_energy'] = tech_data_merged.loc[:,'max_energy'].fillna('inf')
tech_data_merged = tech_data_merged.drop(columns=['max_energy_import'])

#%% Final edits

# Set CO2-price

tech_data_merged['CO2_price'] = 150

# Fill NaN with 0's
tech_data_merged = tech_data_merged.fillna(0)

#%% Save

tech_data_merged.to_csv(
    os.path.join('project_files','data_input','data_input_wide','0_Core','technology_data_upload.csv'),
    index =  False)

#%% STORAGE ########################################################################################

eraa_sto_capas = pd.read_csv(
    os.path.join('data','eraa2021','PECD_EERA2021_reservoir_pumping_2030_country_table.csv'))

eraa_sto_capas.loc[:,'technology'] = eraa_sto_capas.loc[:,'technology'].replace(
    {
        'pumped_closed': 'phs_closed',
        'pumped_open'  : 'phs_open',
        'reservoir'    : 'rsvr'
    })

eraa_sto_capas['value'] = round(eraa_sto_capas['value'],2)

eraa_sto_capas_wide = eraa_sto_capas.pivot(index=['country','technology'], columns='variable', values = 'value').reset_index()
eraa_sto_capas_wide.loc[:,'pumping_cap_MW'] = abs(eraa_sto_capas_wide.loc[:,'pumping_cap_MW'])
eraa_sto_capas_wide.loc[:,'sto_GWh']        = eraa_sto_capas_wide.loc[:,'sto_GWh'] * 1000

#%% Import sto data

sto_data = pd.read_csv(os.path.join(
    'project_files','data_input','data_input_wide','0_Core',
    'storage_data.csv'))

sto_data_merged = sto_data.merge(
    eraa_sto_capas_wide,how='left', left_on = ['n','sto'], right_on = ['country','technology'])

# Set for all hydo tech

sto_data_merged.loc[:,'min_energy']    = sto_data_merged.loc[:,'sto_GWh']
sto_data_merged.loc[:,'max_energy']    = sto_data_merged.loc[:,'sto_GWh']

sto_data_merged.loc[:,'min_power_in']  = sto_data_merged.loc[:,'pumping_cap_MW'] 
sto_data_merged.loc[:,'max_power_in']  = sto_data_merged.loc[:,'pumping_cap_MW'] 

sto_data_merged.loc[:,'min_power_out'] = sto_data_merged.loc[:,'gen_cap_MW']
sto_data_merged.loc[:,'max_power_out'] = sto_data_merged.loc[:,'gen_cap_MW']

# Set for battery and p2g2p

#sto_data_merged.loc[:,'max_energy']    = sto_data_merged.loc[:,'max_energy'].astype(str)
#sto_data_merged.loc[:,'max_power_in']  = sto_data_merged.loc[:,'max_power_in'].astype(str)
#sto_data_merged.loc[:,'max_power_out'] = sto_data_merged.loc[:,'max_power_out'].astype(str)

sto_data_merged.loc[sto_data_merged.loc[:,'sto'].isin(['p2g2p','li-ion']),'min_energy']    = 0
sto_data_merged.loc[sto_data_merged.loc[:,'sto'].isin(['p2g2p','li-ion']),'max_energy']    = 999999999
sto_data_merged.loc[sto_data_merged.loc[:,'sto'].isin(['p2g2p','li-ion']),'min_power_in']  = 0
sto_data_merged.loc[sto_data_merged.loc[:,'sto'].isin(['p2g2p','li-ion']),'max_power_in']  = 999999
sto_data_merged.loc[sto_data_merged.loc[:,'sto'].isin(['p2g2p','li-ion']),'min_power_out'] = 0
sto_data_merged.loc[sto_data_merged.loc[:,'sto'].isin(['p2g2p','li-ion']),'max_power_out'] = 999999

sto_data_merged.loc[sto_data_merged.loc[:,'sto'].isin(['rsvr','phs_open','phs_closed']),'etop_max'] = 9999


# Fill with 0's

sto_data_merged = sto_data_merged.fillna(0)
sto_data_merged = sto_data_merged.replace("nan",0)

# Drop not needed columns

sto_data_merged = sto_data_merged.drop(
    columns=['country','technology','gen_cap_MW','pumping_cap_MW','sto_GWh'])


#%% Save

sto_data_merged.to_csv(
    os.path.join('project_files','data_input','data_input_wide','0_Core','storage_data.csv'),
                        index =  False)
