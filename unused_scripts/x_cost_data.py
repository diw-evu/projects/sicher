#%% Load packages ##################################################################################

import os
import pandas as pd
import numpy as np

#%% ################################################################################################

#%% Import tech_data

tech_data_dieter = pd.read_csv(os.path.join(
    'project_files','data_input','data_input_wide','0_Core',
    'technology_data_upload.csv'))

tech_data_url = "https://ens.dk/sites/ens.dk/files/Analyser/version_12_-_technology_data_for_el_and_dh.xlsx"
tech_data_new = pd.read_excel(tech_data_url,sheet_name="alldata_flat")

#%%

tech_name_dict = {
    '04 Gas turb. simple cycle, L' : 'OCGT',
    '06 Gas engines, biogas' : 'bio',
    '05 Gas turb. CC, steam extract.' : 'CCGT',
    '22 Rooftop PV residential' : 'pv',
    '22 Rooftop PV commercial' : 'pv',
    '22 Utility-scale PV' : 'pv',
    '20 Onshore turbines' : 'wind_on',
    '21 Offshore turbines':'wind_off'
}

par_name_dict = {
    'Technical lifetime':'lifetime',
    'Variable O&M':'variable_om',
    'Fixed O&M':'fixed_costs',
    'Nominal investment':'oc',
    'Electrical efficiency':'eta_con'
}

tech_data_new = tech_data_new.replace({'ws': tech_name_dict}) #, 'par_short': par_name_dict})

#%%

tech = list(tech_name_dict.values())
year = 2030
par  = list(par_name_dict.keys())
est  = 'ctrl'
info = ['*total',np.nan,'net, name plate']

query_string = "ws==@tech & year == @year & par_short == @par & est == @est & info == @info"

tech_filtered  = tech_data_new.query(query_string)

tech_filtered = tech_filtered[['ws','par_short','val']]

tech_filtered.loc[tech_filtered['par_short']=='Nominal investment','val'] *= 1e6

tech_filtered = tech_filtered.groupby(['ws','par_short']).agg({'val':'mean'}).reset_index()

tech_filtered = tech_filtered.pivot(index = 'ws', columns='par_short', values = 'val').reset_index()

tech_filtered['Electrical efficiency'] = tech_filtered['Electrical efficiency'].fillna(1)
tech_filtered['Variable O&M']          = tech_filtered['Variable O&M'].fillna(0)
tech_filtered['Nominal investment']    = tech_filtered['Nominal investment'].astype(int)

#%%

tech_merge = tech_data_dieter.merge(tech_filtered,how = "left", left_on=['tech'], right_on=['ws'])

# replace values

tech_merge.loc[tech_merge['tech'].isin(tech),'lifetime']    = tech_merge.loc[tech_merge['tech'].isin(tech),'Technical lifetime']
tech_merge.loc[tech_merge['tech'].isin(tech),'variable_om'] = tech_merge.loc[tech_merge['tech'].isin(tech),'Variable O&M']
tech_merge.loc[tech_merge['tech'].isin(tech),'fixed_costs'] = tech_merge.loc[tech_merge['tech'].isin(tech),'Fixed O&M']
tech_merge.loc[tech_merge['tech'].isin(tech),'oc']          = tech_merge.loc[tech_merge['tech'].isin(tech),'Nominal investment']
tech_merge.loc[tech_merge['tech'].isin(tech),'eta_con']    = tech_merge.loc[tech_merge['tech'].isin(tech),'Electrical efficiency']

# save 

drop_columns = list(par_name_dict.keys())
drop_columns.append('ws')

tech_merge = tech_merge.drop(columns=drop_columns)

tech_merge.to_csv(
    os.path.join('project_files','data_input','data_input_wide','0_Core','technology_data_upload.csv'),
    index =  False)

#%% Import tech_data ###############################################################################

sto_data_dieter = pd.read_csv(os.path.join(
    'project_files','data_input','data_input_wide','0_Core',
    'storage_data.csv'))

sto_data_url = "https://ens.dk/sites/ens.dk/files/Analyser/version_06_-_technology_datasheet_for_energy_storage.xlsx"
sto_data_new = pd.read_excel(tech_data_url,sheet_name="alldata_flat")

# continue here ...