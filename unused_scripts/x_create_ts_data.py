#%% Libraries ######################################################################################

import pandas as pd
import numpy as np
import os
import openpyxl
pd.options.plotting.backend = "plotly"

#%% Import from helper module ######################################################################

from helper import *

#%% Create timeseries_input.xlsx ###################################################################

# Select whether one- or two-year period to be used
period_length = 2

# Select whether academic or calendar year used
year_calendar = True

selected_coutries = ['AT','BE','CH','CZ','DE','ES','FI','FR','IE','IT','LU','NL','NO','PL','PT','SE','UK']

# Select countries
#selected_coutries = ['AT','BE','CH','CZ','DE','FR','LU','NL','PL']

# Select base year and iteration years
if year_calendar == True:  
    selected_base_year = 2008
    selected_years = [y for y in range(1982, 2019)]
    
if year_calendar == False:
    selected_base_year = "2008-2009"
    selected_years = [
        "1990-1991",
        "1991-1992",
        "1992-1993",
        "1993-1994",
        "1994-1995",
        "1995-1996",
        "1996-1997",
        "1997-1998",
        "1998-1999",
        "1999-2000",
        "2000-2001",
        "2001-2002",
        "2002-2003",
        "2003-2004",
        "2004-2005",
        "2005-2006",
        "2006-2007",
        "2007-2008",
        "2008-2009",
        "2009-2010",
        "2010-2011"
        ]

#%% Create base data

# Get data - execute function
base_ts = create_timeseries_input(data_dict,selected_base_year,selected_coutries,year_calendar,period_length)

#%% Write excel

# Replace 0 with 'Eps'
#base_ts = base_ts.replace({0:'eps'})

# Select base year

if period_length > 1:
    filename = "timeseries_input_multi.xlsx"

else:
    if year_calendar == True:  
        filename = "timeseries_input_cal.xlsx"
    if year_calendar == False:
        filename = "timeseries_input_aca.xlsx"

# Write to Excel, replace old 'basic' sheet
with pd.ExcelWriter(os.path.join('project_files','data_input',filename), mode='a', if_sheet_exists = 'replace') as writer:  
    base_ts.to_excel(writer, sheet_name='basic', startrow=5, index=False, header=False)

#%% Create iteration data ##########################################################################

# Get data
iteration_data_dict = create_iteration_data(data_dict, selected_years, selected_coutries,
                                            hydro_constant=True,
                                            year_calendar=year_calendar,
                                            period_length=period_length)

# Define Python part
py = {'symbol': ['identifer', 'scenario', 'iter_data', 'h'],
        'sheet_name': ['scenario','scenario','scenario','scenario'], 
        'startcell': ['B4','B5','A4','A6'], 
        'rdim': [0,0,1,1], 
        'cdim':[1,1,2,0],
        'type':['set','set','par','set']
        }

py = pd.DataFrame(data=py)

#%% Write to excel

print("Start saving")
print("----------")

for year in selected_years:
    print(year)
    print("----------")   
    file_path = "project_files/iterationfiles"
    file_name = "iteration_data_" + str(year) + ".xlsx"
    df_to_write = iteration_data_dict[year]
    
    with pd.ExcelWriter(os.path.join(file_path,file_name), mode='w') as writer:
        py.to_excel(writer, sheet_name = 'py', startrow = 0, header = True, index = False)
        df_to_write.to_excel(writer, sheet_name='scenario', startrow = 2, header = True, index = True)

    # delete buggy empty row
    wb = openpyxl.load_workbook(os.path.join(file_path, file_name))
    sheet = wb['scenario']
    sheet.delete_rows(6, 1)
    wb.save(os.path.join(file_path, file_name))

#%% ################################################################################################